#!/usr/bin/env bash

sudo apt update
if ! hash jq 2>/dev/null; then
  sudo apt install -y jq vim curl
fi

#install php
sudo apt install -y lsb-release ca-certificates apt-transport-https software-properties-common gnupg2
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -

sudo apt update
sudo apt install -y php8.0 \
php8.0-curl \
php8.0-bcmath \
php8.0-gd \
php8.0-mbstring \
php8.0-xdebug \
php8.0-xml \
php8.0-zip

sudo cp /provision/xdebug.ini /etc/php/8.0/mods-available/

# install composer
wget -q -O /tmp/composer-setup.php https://getcomposer.org/installer
XDEBUG_MODE=off sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm /tmp/composer-setup.php

# provision the app
sh -c "cd /app && composer install"

